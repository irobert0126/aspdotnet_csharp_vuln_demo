# Code

## Target .Net Framework 4.6.1

## File
- [controller/HomeController.cs::QueryOrder](demo/Controller/HomeController.cs#L105)
- [controller/HomeController.cs::RuntTimeExec](demo/Controller/HomeController.cs#L166)
- [controller/HomeController.cs::insiderHandler](demo/Controller/HomeController.cs#L233)
- [controller/HomeController.cs::AddNewOrderFlaw](demo/Controller/HomeController.cs#L367)


## Visual Studio Solution (Including libraries)

[Dropbox Link](https://www.dropbox.com/s/zsjkci3fo46m9q2/Demo.zip?dl=0)


# Demo
## Register / Login

<img src="Video/reg.gif" width=666 />

## SQL Injection Attack
 
<img src="Video/SqlInjection.gif" width=666 />

## Insider Handler

<img src="Video/InsiderHandler.gif" width=666 />

## Command Injection

<img src="Video/CommandInjection.gif" width=666 />


### Business logic flaw
- [Code Snippet](demo/Controller/HomeController.cs#L367):
    ```java
    public String AddNewOrderFlaw(string reader) {
        Order order = JsonConvert.DeserializeObject<Order>(reader);
        return order.ToString();
    }
    ```